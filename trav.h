void preorder(struct bnode *root);
void inorder(struct bnode *root);
void postorder(struct bnode *root);
void levelorder(struct bnode *root);
