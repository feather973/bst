#define SUCCESS 1
#define FAIL 0

struct bnode {
	struct bnode *left;
	struct bnode *right;
	int key;
};

struct bnode *search(struct bnode *root, int key);
struct bnode *insert(struct bnode *root, int key);
int delete(struct bnode *root, int key);
void deleteall(struct bnode *root);
