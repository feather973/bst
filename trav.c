#include <stdio.h>

#include "bst.h"
#include "queue.h"

void preorder(struct bnode *root)
{
	if (!root)
		return;

	preorder(root->left);
	printf("%d ", root->key);
	preorder(root->right);

	return;
}

void inorder(struct bnode *root)
{
	if (!root)
		return;

	printf("%d ", root->key);
	inorder(root->left);
	inorder(root->right);

	return;
}

void postorder(struct bnode *root)
{
	if (!root)
		return;

	postorder(root->left);
	postorder(root->right);
	printf("%d ", root->key);

	return;
}

void levelorder(struct bnode *root)
{
    struct qhead *head;
    struct bnode *tmp;

    head = createqueue();

    tmp = root;
    while (tmp) {    
        // print
        printf("%d ", tmp->key);

        // enqueue children
        if (tmp->left)
            enqueue(head, tmp->left);

        if (tmp->right)
            enqueue(head, tmp->right);

        // dequeue node
        tmp = dequeue(head);
    }   
}
