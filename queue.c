#include <stdlib.h>
#include <stdio.h>

#include "queue.h"

struct qhead *createqueue(void)
{
	struct qhead *head;
	
	head = calloc(sizeof(struct qhead), 1);
	if (!head) {
		printf("qhead calloc fail\n");
		return NULL;
	}

	return head;
}

int enqueue(struct qhead *head, void *data)
{
	struct qnode *node;

	node = calloc(sizeof(struct qnode), 1);
	if (!node) { 
		printf("qnode calloc fail\n");
		return FAIL;
	}
	node->data = data;			// swallow copy

	if (!head->count) {			// empty queue
		head->front = node;
		head->rear = node;
	} else {
		head->rear->next = node;
		head->rear = node;
	}

	head->count++;

	return SUCCESS;
}

void *dequeue(struct qhead *head)
{
	struct qnode *node;
	void *data;

	if (!head->count)			// empty queue
		return NULL;

	if (head->count == 1) {		// single node
		node = head->front;

		head->count = 0;
		head->front = NULL;
		head->rear = NULL;
	} else {
		node = head->front;

		head->count--;
		// front (node->next is valid because head->count is 2)
		head->front = node->next;
		// rear
		// do nothing
	}

	data = node->data;
	free(node);

	return data;
}

void destroyqueue(struct qhead *head)
{
	int i;
	int cnt = head->count;

	for (i=0; i<cnt; i++)
		dequeue(head);

	free(head);
}
