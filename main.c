#include <stddef.h>
#include <stdio.h>

#include "bst.h"
#include "trav.h"

void check(struct bnode *root)
{
	printf("preorder : ");
	preorder(root);
	printf("\n");
	printf("level : ");
	levelorder(root);
	printf("\n\n");
}

int main()
{
	struct bnode *root;

	root = insert(NULL, 10); 
	insert(root, 20);
	insert(root, 1023);
	insert(root, 0);
	insert(root, 3213);
	insert(root, 10024);
	insert(root, 34);
	insert(root, 35);
	insert(root, 55);
	insert(root, 421);
	check(root);

	delete(root, 1023);
	insert(root, 1000);
	insert(root, 1001);
	insert(root, 111);
	check(root);

	printf("deleteall\n");
	deleteall(root);
	return 0;
}
