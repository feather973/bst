#include <stdio.h>
#include <stdlib.h>

#include "bst.h"

// 재귀 search
struct bnode *search(struct bnode *root, int key)
{
	if (!root) {
		printf("root is NULL\n");
		return NULL;
	}

	if (root->key < key)
		return search(root->right, key);

	if (root->key > key)
		return search(root->left, key);

	return root;
}

// 반복에 의한 삽입
struct bnode *insert(struct bnode *root, int key)
{
	struct bnode *pWalk, *parent, *new;

	if (!root) {
		root = calloc(sizeof(struct bnode), 1);
		if (!root) {
			printf("calloc fail\n");
			return NULL;
		}
	}

	// 1. 부모를 찾는다.
	pWalk = root;
	while (pWalk) {
		parent = pWalk;		// update parent

		// left 부터
		if (pWalk->key > key) {
			pWalk = pWalk->left;
		} else if (pWalk->key < key) {
			pWalk = pWalk->right;
		} else {	// error
			printf("duplicate key\n");
			return NULL;
		}
	}

	// 2. 삽입
	new = calloc(sizeof(struct bnode), 1); 
	if (!new) {
		printf("calloc fail\n");
		return NULL;
	}
	new->key = key;

	if (parent->key > key)
		parent->left = new;
	else
		parent->right = new;

	return new;
}

// 삭제시엔 부모가 필요, 반복
int delete(struct bnode *root, int key)
{
	struct bnode *pWalk, *parent, *del_bnode, *left_most;

	if (!root) {
		return FAIL;
	}

	// 1. 부모를 찾는다.
	pWalk = parent = root;		// parent 초기화 필요
	while (pWalk && (pWalk->key != key)) {		// 없는 키인 경우 pWalk == NULL
		parent = pWalk;		// update parent

		// left 부터
		if (pWalk->key > key) {
			pWalk = pWalk->left;
		} else if (pWalk->key < key) {
			pWalk = pWalk->right;
		}
	}

	// not found
	if (!pWalk) {
		printf("not found\n");
		return FAIL;
	}

	// found
	del_bnode = pWalk;

	if (!del_bnode->left && !del_bnode->right) {	// leaf bnode
		if (parent->key > del_bnode->key)
			parent->left = NULL;
		else
			parent->right = NULL;

		free(del_bnode);
		goto success;
	} else if (del_bnode->left && !del_bnode->right) {	// left sub-tree
		if (parent->key > del_bnode->key)
			parent->left = del_bnode->left;
		else
			parent->right = del_bnode->left;

		free(del_bnode);
		goto success;
	} else if (!del_bnode->left && del_bnode->right) {	// right sub-tree
		if (parent->key > del_bnode->key) {
			parent->left = del_bnode->right;
		}
		else {
			parent->right = del_bnode->right;
		}

		free(del_bnode);
		goto success;
	} else {	// both sub-tree
		struct bnode *left_most_parent;

		// initial value 
		left_most_parent = del_bnode;
		left_most = del_bnode->left;

		while (left_most->right) {
			left_most_parent = left_most;
			left_most = left_most->right;
		}

		left_most_parent->right = NULL;
		del_bnode->key = left_most->key;

		free(left_most);
	}

success:
	return SUCCESS;
}

// post order
void deleteall(struct bnode *root)
{
	if (!root)
		return;

	deleteall(root->left);
	deleteall(root->right);
	printf("%d\n", root->key);		// for debug
	free(root);

	return;
}
