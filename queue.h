#define FAIL 0
#define SUCCESS 1

struct qhead {
	struct qnode *front; 
	struct qnode *rear;

	int count;
};

struct qnode {	
	void *data;	
	struct qnode *next;
};

struct qhead *createqueue(void);
int enqueue(struct qhead *head, void *data);
void *dequeue(struct qhead *head);
void destroyqueue(struct qhead *head);
